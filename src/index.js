import App from "./app.svelte";

const defaultState = {
  timers: [
    { name: "Pomodoro", time: 25 },
    { name: "Short Break", time: 5 }
  ]
};

const app = new App({
  props: {
    state: localStorage.getItem("state")
      ? JSON.parse(localStorage.getItem("state"))
      : defaultState
  },
  target: document.body
});
